% An example of a shared preamble.
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{multidocument-example}[2023/03/21 Example of a shared preamble.]

\RequirePackage{kvoptions}

\DeclareDefaultOption{\PassOptionsToClass{\CurrentOption}{multidocument}}

\ProcessOptions*\relax

\LoadClass[baseclass=article]{multidocument}

% Font loading.
\RequirePackage{fontspec}
\RequirePackage{microtype}
% Custom section styling.
\RequirePackage{titlesec}

% Set the sans font to Futura.
\setmainfont[Ligatures=TeX]{Palatino}
\setsansfont[
    Ligatures = TeX,
    Scale = MatchLowercase,
]{Gill Sans}

\newfontfamily{\futura}[
    Ligatures = TeX,
    Scale = MatchUppercase,
]{Futura}

\titleformat{\section}{\futura\Large\bfseries\MakeUppercase}{\thesection}{1em}{}
\titleformat{\subsection}{\futura\large\bfseries}{\thesubsection}{1em}{}


\RequirePackage{csquotes}

% \titleformat{\chapter}[display]{\normalfont\huge\bfseries}{\chaptertitlename\ \thechapter}{20pt}{\Huge}
% \titleformat{\section}{\normalfont\Large\bfseries}{\thesection}{1em}{}
% \titleformat{\subsection}{\normalfont\large\bfseries}{\thesubsection}{1em}{}
% \titleformat{\subsubsection}{\normalfont\normalsize\bfseries}{\thesubsubsection}{1em}{}
% \titleformat{\paragraph}[runin]{\normalfont\normalsize\bfseries}{\theparagraph}{1em}{}
% \titleformat{\subparagraph}[runin]{\normalfont\normalsize\bfseries}{\thesubparagraph}{1em}{}
% \titlespacing*{\chapter} {0pt}{50pt}{40pt}
% \titlespacing*{\section} {0pt}{3.5ex plus 1ex minus .2ex}{2.3ex plus .2ex}
% \titlespacing*{\subsection} {0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}
% \titlespacing*{\subsubsection}{0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}
% \titlespacing*{\paragraph} {0pt}{3.25ex plus 1ex minus .2ex}{1em}
% \titlespacing*{\subparagraph} {\parindent}{3.25ex plus 1ex minus .2ex}{1em}

% TItle of a book.
\NewDocumentCommand{\booktitle}{ m }{\emph{#1}}

% Name of the game system.
\NewDocumentCommand{\systemname}{}{System Name}
\NewDocumentCommand{\thissystem}{}{\booktitle{\systemname}}

\RequirePackage{thecomplex}
\RequirePackage{fitd}
\endinput